
public class Zug
{
	private Wagon erster;
	private Wagon letzter;
	private int zuglaenge = 0;
	public Zug ()
	{
		
	}
	
	public void neuerWagon(String inhalt)
	{
		Wagon neuerWagon = new Wagon(inhalt);
		//der Zug ist leer
		if(erster == null) // null==> die referenz zeigt nich ins leere
		{
			erster=neuerWagon;
			letzter=neuerWagon;
		}
		else  // es gib zumindest einen Wagon 
		{
			letzter.einhaegen(neuerWagon);
			letzter=neuerWagon;
		}
		zuglaenge++;
	}
	
	// gib die inhalt von bestimmte Wagon
	public String wagonInhaltAnStelle(int stelle)
	{  
		if(stelle>zuglaenge -1 || stelle < 0 ) //pr�fen ob die Stelle vorhanden ist 
		{
			return "Stelle nicht vorhanden";
		}
		Wagon temp=erster; // von der ersten Wagon beginnen
		
		
		for (int springen=0; springen< stelle ; springen++)
		{
			temp= temp.Nachbar(); // zum n�chsten springen
		}
		return temp.toString();
	}
	public int length()
	{
		return zuglaenge;
	}
	
	public String toString()
	{
		if (erster == null) //kein wagon gibt
		{
			return "0";
			
		}
		else
		{
			String ergebnis = "es gibt " + zuglaenge + " Wagon: \n";
			Wagon temp = erster;
			while (temp != null)
			{
				ergebnis =  ergebnis + " " +temp.toString();
				temp=temp.Nachbar();
			}
			return ergebnis;
		}
	}
	

}
